import {Module, Logger} from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { OkyanusModule } from "../okyanus-controller/okyanus.module";
@Module({
  imports: [ConfigService, Logger, OkyanusModule],
  controllers: []
})
export class AppModule {

}
