import { Test, TestingModule } from '@nestjs/testing';
import { OkyanusControllerController } from './okyanus-controller.controller';

describe('OkyanusController Controller', () => {
  let controller: OkyanusControllerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OkyanusControllerController],
    }).compile();

    controller = module.get<OkyanusControllerController>(OkyanusControllerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
