import { Module, Logger } from "@nestjs/common";
import { OkyanusController } from "./okyanus-controller.controller";
import { OkyanusService } from "./okyanus.service";
import {AuthModule} from "../auth/auth.module";

@Module({
    controllers: [OkyanusController],
    providers: [OkyanusService, Logger],
    imports: [AuthModule]
})
export class OkyanusModule {}
