import {Controller, Get, Query, UseGuards, Post, Request} from '@nestjs/common';
import {LocalAuthGuard} from "../auth/local-auth.guard";
import {AuthService} from "../auth/auth.service";
import {JwtAuthGuard} from "../auth/jwt-auth.guard";
@Controller('okyanus-controller')
export class OkyanusController {
    constructor(private authService: AuthService) {}
    @UseGuards(JwtAuthGuard)
    @Get()
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    get(@Query() params: any) {
        const {x = '#', y = ' ', n = 8} = params;
        if(n <=0 || Number.isNaN(n)) return '';
        return this.createResponse(x, y, n);
    }

    createResponse(x: string, y: string, n: number) {
        let result = '';
        for (let i = 0; i < n; i++) {
            result = result.concat('\n'); // if one wants to get fancy, can check the index and ignore this like if i===0,
            // imo it's unnecessary for this function.
            result = result.concat(this.createARow(x, y, true, n));
        }
        return result;
    }

    createARow(x: string, y: string, startWithX: boolean, n: number) {
        let nextCharIsX = startWithX;
        let str = '';
        for (let i = 0; i < n; i++) {
            str = nextCharIsX ? str.concat(x) : str.concat(y);
            nextCharIsX = !nextCharIsX
        }
        return str;
    }

    @UseGuards(LocalAuthGuard)
    @Post('auth/login')
    async login(@Request() req: any) {
        return this.authService.login(req.user);
    }

}
